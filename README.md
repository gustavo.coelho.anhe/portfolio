<h1 align="center">Portfolio</h1>

<p align="center">
  <img alt="Github top language" src="https://img.shields.io/github/languages/top/w1redl4in/portfolio" />
  <img alt="Github last commit" src="https://img.shields.io/github/last-commit/w1redl4in/portfolio" />
</p>

![App Screenshot](https://media.discordapp.net/attachments/693737591606083654/989431746451877908/unknown.png?width=1377&height=676)

<p align="center">
  <a href="https://www.coelhoviski.com/" target="_blank">View here</a>
</p>

## :fire: Ignite Rocketseat Journey

My portfolio built with Nextjs and deployed by Vercel.

## :rocket: Technologies

- [Nextjs](https://nextjs.org/)
- [styled-components](https://styled-components.com/)
- [TypeScript](https://www.typescriptlang.org/)

---

Edited by Gustavo Coelho :wave: [Get in touch!](https://www.linkedin.com/in/gustavo-coelho-808937184/)


