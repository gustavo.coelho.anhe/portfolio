import { useMemo } from "react";
import {
  AiFillGithub,
  AiFillLinkedin,
  AiFillInstagram,
} from "react-icons/ai";

export function useNavbarItems() {
  const navBarItems = useMemo(() => {
    return [
      {
        href: "https://www.instagram.com/coelhoviski/",
        component: <AiFillInstagram />,
      },
      {
        href: "https://www.linkedin.com/in/gustavo-coelho-808937184/",
        component: <AiFillLinkedin />,
      },
      {
        href: "https://github.com/Ciberox",
        component: <AiFillGithub />,
      },

    ];
  }, []);

  return { navBarItems };
}
