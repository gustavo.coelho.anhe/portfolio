import {
  FooterContainer,
  SocialMedias,
  Contact,
  ScrollToTopButton,
} from "./styles";
import { AiOutlineWhatsApp, AiOutlineMail } from "react-icons/ai";
import { FaArrowCircleUp } from "react-icons/fa";
import { useNavbarItems } from "../../hooks/use-navbar-items";

export const Footer: React.FC = () => {
  const { navBarItems } = useNavbarItems();

  return (
    <FooterContainer>
      <h4>Edited by Gustavo Coelho Anhê, but who really did this &nbsp;
         <u>shit</u> is Felipe Austríaco</h4>
      <SocialMedias>
        {navBarItems.map((navBarItem) => {
          return navBarItem.component ? (
            <a key={navBarItem.href} href={navBarItem.href} target="_blank">
              {navBarItem.component}
            </a>
          ) : (
            <a key={navBarItem.href} href={navBarItem.href} target="_blank">
            </a>
          );
        })}
      </SocialMedias>
      <Contact>
        <p>
          <AiOutlineMail /> Gustavo.coelho.anhe@gmail.com
        </p>
        <p>
          <AiOutlineWhatsApp /> (11) 99341-6949
        </p>
      </Contact>
      <ScrollToTopButton to="initial" smooth>
        Take a shortcut
        <FaArrowCircleUp />
      </ScrollToTopButton>
    </FooterContainer>
  );
};
