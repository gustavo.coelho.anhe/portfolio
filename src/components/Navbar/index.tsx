import { NavbarContainer, NavInfo, Date as Time } from "./styles";
import { useEffect, useState } from "react";
import { useNavbarItems } from "../../hooks/use-navbar-items";

export const Navbar: React.FC = () => {
  const [scrolledToTheBottom, setScrolledToTheBottom] = useState(false);
  const { navBarItems } = useNavbarItems();

  useEffect(() => {
    window.onscroll = function () {
      if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        setScrolledToTheBottom(true);
      } else setScrolledToTheBottom(false);
    };
  }, []);

  return (
    <NavbarContainer scrolledToTheBottom={scrolledToTheBottom}>
      <div>
        <img
          src="https://avatars.githubusercontent.com/u/55093443?s=400&u=50df0ab26518139c8655aa41f8328e5f649e03c9&v=4"
          alt="Gus"
        />
        <NavInfo>
          {navBarItems.map((navBarItem) => {
            return navBarItem.component ? (
              <a key={navBarItem.href} href={navBarItem.href} target="_blank">
                {navBarItem.component}
              </a>
            ) : (
              <a key={navBarItem.href} href={navBarItem.href} target="_blank">
                
              </a>
            );
          })}
        </NavInfo>
        <Time>{new Date().getFullYear()}</Time>
      </div>
    </NavbarContainer>
  );
};
